include $(TOPDIR)/rules.mk

PKG_NAME:=smsport
PKG_VERSION:=1.0
PKG_RELEASE:=0

PKG_BUILD_DIR := $(BUILD_DIR)/$(PKG_NAME)-$(PKG_VERSION)-$(PKG_RELEASE)/port


include $(INCLUDE_DIR)/package.mk

define Package/smsport
  SUBMENU:=Captive Portals
  SECTION:=net
  CATEGORY:=Network
  TITLE:=smsport
  MAINTENER:=D.L
endef


define Package/port/description
	smsport for eotu AT
endef

define Build/Prepare
	mkdir -p $(PKG_BUILD_DIR)
	$(CP) ./src/*  $(PKG_BUILD_DIR)
	$(CP) ./*  $(BUILD_DIR)/$(PKG_NAME)-$(PKG_VERSION)-$(PKG_RELEASE)
endef

define Package/smsport/install
	$(INSTALL_DIR) $(1)/usr/bin
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/smsport $(1)/usr/bin/
endef

$(eval $(call BuildPackage,smsport))

